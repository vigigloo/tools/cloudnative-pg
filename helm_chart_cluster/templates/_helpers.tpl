{{/* vim: set filetype=mustache: */}}

{{- define "local.ownerSecretName" }}{{ include "common.fullname" . }}-owner{{ end }}
{{- define "local.rootSecretName" }}{{ include "common.fullname" . }}-superuser{{ end }}
