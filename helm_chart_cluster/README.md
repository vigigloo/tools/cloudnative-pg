# Helm chart **cloudnative-pg-cluster**

## Parameters

### Global parameters

| Name                             | Description                                                                                                                                              | Value |
| -------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- | ----- |
| `replicaCount`                   | represents the number of pods that will be created for the application                                                                                   | `1`   |
| `nameOverride`                   | represents the name that should override the default naming of the application                                                                           | `""`  |
| `fullnameOverride`               | represents the full name that should override the default full naming of the application                                                                 | `""`  |
| `bootstrap.databaseName:`        | Database name that will be created on init.                                                                                                              |       |
| `bootstrap.ownerSecret:`         | Name of the secret to use for owner, that have `password` and `username` keys, have priority over `bootstrap.ownerPassword` and `bootstrap.ownerName`.   |       |
| `bootstrap.ownerName:`           | Name of the user that will be owner of the created database.                                                                                             |       |
| `bootstrap.ownerPassword:`       | Password of the owner of the database.                                                                                                                   |       |
| `bootstrap.rootSecret:`          | Name of the secret to use for root user, that have `password` and `username` keys, have priority over `bootstrap.rootPassword` and `bootstrap.rootName`. |       |
| `bootstrap.rootSecret:`          | Name of the superuser.                                                                                                                                   |       |
| `bootstrap.rootName:`            | Name of the superuser.                                                                                                                                   |       |
| `bootstrap.rootPassword:`        | Password of the superuser.                                                                                                                               |       |
| `cluster.description:`           | A brief description of the cluster's purpose or characteristics.                                                                                         |       |
| `cluster.description:`           | A brief description of the cluster's purpose or characteristics.                                                                                         |       |
| `cluster.imageName:`             | The name of the Docker image to use for the cluster.                                                                                                     |       |
| `cluster.imagePullSecrets.name:` | Kubernetes secret to use for pulling images from a private registry.                                                                                     |       |
| `cluster.startDelay:`            | The delay in seconds before the cluster starts.                                                                                                          |       |
| `cluster.stopDelay:`             | The delay in seconds before the cluster stops.                                                                                                           |       |
| `cluster.primaryUpdateStrategy:` | Strategy for updating the primary node in the cluster.                                                                                                   |       |

### PostgreSQL Configuration

| Name                        | Description       | Value |
| --------------------------- | ----------------- | ----- |
| `postgresql.parameters:`    | custom parameters |       |
| `postgresql.parameters.max` |                   |       |
| `postgresql.pg_hba`         |                   |       |

### Storage and Backup Configuration

| Name                    | Description                    | Value |
| ----------------------- | ------------------------------ | ----- |
| `storage.size:`         | Size of the postgresql storage |       |
| `storage.storageClass:` | Class of the storage           |       |

### Resource Management

| Name        | Description | Value |
| ----------- | ----------- | ----- |
| `resources` |             |       |

### Affinity and Anti-Affinity

| Name       | Description | Value |
| ---------- | ----------- | ----- |
| `affinity` |             |       |

### Node Maintenance

| Name                    | Description | Value |
| ----------------------- | ----------- | ----- |
| `nodeMaintenanceWindow` |             |       |
